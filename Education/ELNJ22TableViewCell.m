//
//  ELNJ22TableViewCell.m
//  Education
//
//  Created by Geor Kasapidi on 14.03.16.
//  Copyright © 2016 ELN. All rights reserved.
//

#import "ELNJ22TableViewCell.h"
#import "ELNLabel.h"
#import "ELNJ22CellDemoModel.h"

@interface ELNJ22TableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *pictureView;

@property (weak, nonatomic) IBOutlet ELNLabel *topLabel;
@property (weak, nonatomic) IBOutlet ELNLabel *bottomLabel;

@end

@implementation ELNJ22TableViewCell

- (void)setDemoModel:(ELNJ22CellDemoModel *)demoModel
{
    _demoModel = demoModel;
    
    self.pictureView.image = demoModel.picture;
    self.topLabel.text = demoModel.topText;
    self.bottomLabel.text = demoModel.bottomText;
}

@end
