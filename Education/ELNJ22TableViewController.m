//
//  ELNJ22TableViewController.m
//  Education
//
//  Created by Geor Kasapidi on 14.03.16.
//  Copyright © 2016 ELN. All rights reserved.
//

#import "ELNJ22TableViewController.h"
#import "ELNJ22CellDemoModel.h"
#import "ELNJ22TableViewCell.h"

@interface ELNJ22TableViewController ()

@property (strong, nonatomic) NSArray<ELNJ22CellDemoModel *> *items;

@end

@implementation ELNJ22TableViewController

- (UITabBarItem *)tabBarItem
{
    return [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemHistory tag:1];
}

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Table View Demo";
    
    ELNJ22CellDemoModel *model1 = [ELNJ22CellDemoModel new];
    
    model1.picture = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cell_picture_1" ofType:@"jpeg"]];
    model1.topText = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit";
    model1.bottomText = @"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    
    ELNJ22CellDemoModel *model2 = [ELNJ22CellDemoModel new];
    
    model2.picture = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"cell_picture_2" ofType:@"jpeg"]];
    model2.topText = @"Sed ut perspiciatis unde omnis iste natus error";
    model2.bottomText = @"Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.";
    
    self.items = @[model1, model2];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ELNJ22TableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ELNJ22TableViewCell class])];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ELNJ22TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ELNJ22TableViewCell class]) forIndexPath:indexPath];
    
    cell.demoModel = self.items[indexPath.row];
    
    return cell;
}

@end
