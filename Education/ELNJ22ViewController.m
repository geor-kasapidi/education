//
//  ELNJ22ViewController.m
//  Education
//
//  Created by Geor Kasapidi on 14.03.16.
//  Copyright © 2016 ELN. All rights reserved.
//

#import "ELNJ22ViewController.h"

@interface ELNJ22ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *demoImageView;
@property (weak, nonatomic) IBOutlet UIButton *demoButton;

@end

@implementation ELNJ22ViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    return [self initWithDemoObject:[NSObject new]];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    return [self initWithDemoObject:[NSObject new]];
}

- (instancetype)initWithDemoObject:(NSObject *)demoObject
{
    return [super initWithNibName:NSStringFromClass([ELNJ22ViewController class]) bundle:nil];
}

#pragma mark - Tab Bar

- (UITabBarItem *)tabBarItem
{
    return [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemTopRated tag:0];
}

#pragma mark - Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Main Demo";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:nil action:nil];
    
    self.demoImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"demo" ofType:@"jpg"]];
    
    [self.demoButton setBackgroundImage:[[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"demo_cap_insets" ofType:@"png"]] resizableImageWithCapInsets:UIEdgeInsetsMake(8, 8, 8, 8) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
    
    [self subscribeToKeyboardSystemNotifications];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Date Actions

- (IBAction)date1Tapped:(id)sender
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    dateFormatter.dateFormat = @"dd MMM yyyy";
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:dateString preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)date2Tapped:(id)sender
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    
    dateFormatter.dateFormat = @"HH:mm dd-MM-yyyy";
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:dateString preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)date3Tapped:(id)sender
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    
    dateFormatter.dateStyle = NSDateFormatterFullStyle;
    dateFormatter.doesRelativeDateFormatting = YES;
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:-36*60*60]];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:dateString preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Keyboard Helpers

- (void)subscribeToKeyboardSystemNotifications
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"%@ - %@", UIKeyboardWillShowNotification, note);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardDidShowNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"%@ - %@", UIKeyboardDidShowNotification, note);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"%@ - %@", UIKeyboardWillHideNotification, note);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardDidHideNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"%@ - %@", UIKeyboardDidHideNotification, note);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillChangeFrameNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"%@ - %@", UIKeyboardWillChangeFrameNotification, note);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardDidChangeFrameNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"%@ - %@", UIKeyboardDidChangeFrameNotification, note);
    }];
}

#pragma mark - Actions

- (IBAction)imageTapped:(id)sender
{
    if (self.demoImageView.contentMode == UIViewContentModeScaleToFill) {
        self.demoImageView.contentMode = UIViewContentModeScaleAspectFill;
    } else if (self.demoImageView.contentMode == UIViewContentModeScaleAspectFill) {
        self.demoImageView.contentMode = UIViewContentModeScaleAspectFit;
    } else {
        self.demoImageView.contentMode = UIViewContentModeScaleToFill;
    }
}

#pragma mark - Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

@end
