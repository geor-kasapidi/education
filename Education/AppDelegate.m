//
//  AppDelegate.m
//  Education
//
//  Created by Geor Kasapidi on 14.03.16.
//  Copyright © 2016 ELN. All rights reserved.
//

#import "AppDelegate.h"

#import "ELNJ22TabBarController.h"
#import "ELNJ22NavigationController.h"
#import "ELNJ22ViewController.h"
#import "ELNJ22TableViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [UIWindow new];
    ELNJ22TabBarController *tabsVC = [ELNJ22TabBarController new];
    tabsVC.viewControllers = @[[[ELNJ22NavigationController alloc] initWithRootViewController:[ELNJ22ViewController new]],
                               [[ELNJ22NavigationController alloc] initWithRootViewController:[ELNJ22TableViewController new]]];
    self.window.rootViewController = tabsVC;
    tabsVC.selectedIndex = 0;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
