//
//  ELNJ22TabBarController.m
//  Education
//
//  Created by Geor Kasapidi on 14.03.16.
//  Copyright © 2016 ELN. All rights reserved.
//

#import "ELNJ22TabBarController.h"

@interface ELNJ22TabBarController ()

@end

@implementation ELNJ22TabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tabBar.translucent = NO;
    self.tabBar.tintColor = [UIColor whiteColor];
    self.tabBar.barTintColor = [UIColor purpleColor];
    self.tabBar.shadowImage = [UIImage new];
    self.tabBar.backgroundImage = [UIImage new];
}

@end
