//
//  ELNJ22ViewController.h
//  Education
//
//  Created by Geor Kasapidi on 14.03.16.
//  Copyright © 2016 ELN. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ELNJ22ViewController : UIViewController

- (instancetype)initWithDemoObject:(NSObject *)demoObject NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
