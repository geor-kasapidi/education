//
//  ELNLabel.m
//  Education
//
//  Created by Geor Kasapidi on 14.03.16.
//  Copyright © 2016 ELN. All rights reserved.
//

#import "ELNLabel.h"

@implementation ELNLabel

- (void)layoutSubviews
{
    if (self.numberOfLines == 0) {
        self.preferredMaxLayoutWidth = CGRectGetWidth(self.frame);
    }
    
    [super layoutSubviews];
}

@end
