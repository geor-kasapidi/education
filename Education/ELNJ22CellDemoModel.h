//
//  ELNJ22CellDemoModel.h
//  Education
//
//  Created by Geor Kasapidi on 14.03.16.
//  Copyright © 2016 ELN. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ELNJ22CellDemoModel : NSObject

@property (strong, nonatomic) UIImage *picture;
@property (copy, nonatomic) NSString *topText;
@property (copy, nonatomic) NSString *bottomText;

@end

NS_ASSUME_NONNULL_END
