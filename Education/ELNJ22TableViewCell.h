//
//  ELNJ22TableViewCell.h
//  Education
//
//  Created by Geor Kasapidi on 14.03.16.
//  Copyright © 2016 ELN. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ELNJ22CellDemoModel;

NS_ASSUME_NONNULL_BEGIN

@interface ELNJ22TableViewCell : UITableViewCell

@property (strong, nonatomic) ELNJ22CellDemoModel *demoModel;

@end

NS_ASSUME_NONNULL_END
